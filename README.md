# Picking Up the Pieces, Issues And Challenges Controlling Your Data
## Transitioning from a DEV to an Operational Centric DBA environment 

The primary task of a developer is to get the product out the door. Sometimes, making deadlines means taking shortcuts. Eventually, all those quick and easy tricks become so convoluted and complex that this house of cards risks collapsing with the first stiff breeze. In other words Technical Debt has been created and must now be managed.

Over time, the possibility of slowing down, even reversing one's success risks becoming a self-inflicting nightmare. A different path needs to be charted and navigated before it's too late.

Enter the new guy... a new kind of DBA.

This presentation describes a modern view of the DBA in an environment that has grown heavy and unwieldy as TECHNICAL DEBT becomes ever more expensive. We'll see how he can play a new and ambitious role resolving these issues with a perspective unique to the database administrator.

Valuable Methods and techniques covered includes:

    Figuring out what's going on
        Monitoring the system
        Developing metrics that everyone understands
        Correcting bottlenecks to critical issues
        Performance tuning (and the law of diminishing returns)
    Streamlining the infrastructure
        The QA/DEV/PROD development cycle
        Working better with what you have (having money doesn't help)
        Improving the infrastructure (sometime money does make a difference)
    Documentation
        Leveraging what you write
        The art of creating, assigning and documenting tasks
    Empowering the DEV
        RDBMS Mentoring
        Best Practices, Dos and Don'ts
        Guiding developers into the world of DEVOPS
        Preparing and supporting the ON CALL developer
